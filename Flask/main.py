from flask import Flask
from flask import request
from flask import render_template

app = Flask(__name__)

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)

@app.route('/helloGet/')
@app.route('/helloGet/<name>')
def helloGet(name=""):
    print(name)
    return {'name':name}

@app.route('/helloPost/', methods=['POST'])
def helloPost():
    body = request.json
    return body['value']

app.run(host='0.0.0.0')