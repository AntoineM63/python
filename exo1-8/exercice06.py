def factorial(number):
    i = 1
    res = 1
    if number > 0:
        while i <= number:
            res = res * i
            i = i+1
    else:
        while i >= number:
            res = res * i
            i = i-1
    return res

def run():
    assert factorial(1) == 1
    assert factorial(2) == 2
    assert factorial(3) == 6
    assert factorial(4) == 24
    assert factorial(8) == 40320
    assert factorial(-8) == -40320