import pygame
from pygame import display
from game import Game

def main():

    pygame.init()

    screen = display.set_mode((800, 600))
    
    game = Game()
    game.init(screen)

    while game.should_run:

        screen.fill((0, 0, 0))

        game.update()

        display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game.should_run = False

# On exécute la fonction main() uniquement si on lance le fichier main.py
# Si on importe le fichier main.py dans un autre fichier, __name__ ne
# sera pas égal à "__main__" et on n'exécutera pas le jeu
if __name__ == "__main__":
    main()