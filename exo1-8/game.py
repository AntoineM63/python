import pygame
from pygame import draw, display

pygame.init()
running = True

screen = display.set_mode((800, 600))
(x, y) = (0, 300)

while running:
    
    screen.fill((0, 0, 0))

    draw.circle(
        screen,
        (255, 255, 255),
        (x, y),
        15
    )
    if x < 785:
        x += 1
    else: x -= 1

    display.update()