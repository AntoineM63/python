a = 1
b = 2

x = a
a = b
b = x

def run():
    assert a == 2
    assert b == 1
