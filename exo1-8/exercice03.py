def is_product_negative(a, b):
    if a < 0 and b < 0:
        res = False
    elif a < 0:
        res = True
    elif b < 0:
        res = True
    else:
        res = False
    return res

def run():
    assert is_product_negative(6, 7) == False
    assert is_product_negative(1, 0) == False
    assert is_product_negative(-1, 5) == True
    assert is_product_negative(1, -5) == True
    assert is_product_negative(-1, -5) == False
